#include "Angel.h"
#include "pNoise.h"
#include <vector>
#include <fstream>
#include <sstream>
#include <assert.h>
#include <stb_image.c>

#define sizeofVec(stdVector) stdVector.size()*sizeof(stdVector[0])

//- CONSTANTS --------------------------------------------------------------
#define WINDOW_WIDTH    500
#define WINDOW_HEIGHT   500

#define X_AXIS      0
#define Y_AXIS      1
#define Z_AXIS      2

#define MAX_RES     100

enum { X = 0, Y = 1, Z = 2 };

// ASCII KEYS CODE
#define KEY_ESC 033
#define KEY_SPC  32

struct vbo_t {
    std::vector<vec4> points;
    std::vector<vec3> colors;
    std::vector<vec3> normals;
    std::vector<vec3> normalsFace;
    std::vector<vec2> textureCoords;
};

// Control points
vec4 controlPoints[16];
vec3 colorCP[16];

// Axis
vec4 axisPoints[6] = {
    vec4(0.0, 0.0, 0.0, 1.0),
    vec4(1.0, 0.0, 0.0, 1.0),
    vec4(0.0, 0.0, 0.0, 1.0),
    vec4(0.0, 1.0, 0.0, 1.0),
    vec4(0.0, 0.0, 0.0, 1.0),
    vec4(0.0, 0.0, 1.0, 1.0)
};
vec3 axisColors[6] = {
    vec3(1.0, 0.0, 0.0),
    vec3(1.0, 0.0, 0.0),
    vec3(0.0, 1.0, 0.0),
    vec3(0.0, 1.0, 0.0),
    vec3(0.0, 0.0, 1.0),
    vec3(0.0, 0.0, 1.0)
};

const int textureSize  = 64;
// const int textureSize  = 512;
GLubyte image[textureSize][textureSize][3];
unsigned char *imageFile;
unsigned char *imageNormFile;
unsigned char *imageHeightFile;
int textWidth;
int textHeight;
int textComp;


//- GLOBALS ----------------------------------------------------------------
vbo_t mesh, normalArray;
GLuint meshBuffer, controlPointsBuffer, axisBuffer;
GLuint normalBuffer, normFaceBuffer;

GLuint wireFrameShader, smoothShadingShader, flatShadingShader;

// Viewing transformation parameters
GLfloat radius = 10.0;
GLfloat theta  = 0.0;
GLfloat phi    = 0.0;

// Projection transformation parameters (Perspective)
GLfloat  fovy = 45.0;  // Field-of-view in Y direction angle (in degrees)
GLfloat  aspect;       // Viewport aspect ratio
GLfloat  zNear  = 0.005, zFar  = 300.0;

// IDs of shader variable locations
GLuint idModelView, idProjection;
GLuint idvPosition, idvColor, idvNormal, idvNormFace, idvTextCoord;
GLuint idEyePos, idDisplacement, idEnableDisp, idEnableBump;

// mouse camera control
int lastMx = 0, lastMy = 0;
bool rotateCamera = false;
bool zoomCamera = false;

int resolution = 100;
int markedControlPoint = 5;

bool shading = true;
bool flatShading = false;
bool showNormals = false;

float displacement = 0.5;
float enableDisp = 0.0;
float enableBump = 0.0;

inline void addVertex( vec4 vertex, vec3 norm, vec3 normFace, vec2 textCoord )
{
    mesh.points.push_back(vertex);
    mesh.normals.push_back(norm);
    mesh.normalsFace.push_back(normFace);
    mesh.colors.push_back(vec3(1.0, 1.0, 1.0));
    mesh.textureCoords.push_back(textCoord);
}

float bernstein(int i, float u )
{
    switch ( i ) {
        case 0: return pow((1-u),3);
        case 1: return 3*u*pow((1-u),2);
        case 2: return 3*pow(u,2)*(1-u);
        case 3: return pow(u,3);
    }
    return 0.0f;
}

float bernsteinDeriv(int i, float u )
{
    switch ( i ) {
        case 0: return -3*pow((1-u),2);
        case 1: return 3*pow((1-u),2)-6*u*(1-u);
        case 2: return 6*u*(1-u)-3*pow(u,2);
        case 3: return 3*pow(u,2);
    }
    return 0.0f;
}

void generateMeshFormula()
{

    vec3 vertex[MAX_RES][MAX_RES];
    vec3 normal[MAX_RES][MAX_RES];
    vec2 textureCoords[MAX_RES][MAX_RES];
    vec4 patch[4][4];

    // Initialize each patch's control point data
    for ( int i = 0; i < 4; ++i ) {
        for ( int j = 0; j < 4; ++j ) {
            patch[i][j] = controlPoints[(i*4)+j];
        }
    }

    int steps = resolution-1;
    double invTotalSteps = 1.0f / steps;
    for (int stepU = 0; stepU <= steps; stepU++) {
        float u = stepU * invTotalSteps;
        for (int stepV = 0; stepV <= steps; stepV++) {
            float v = stepV * invTotalSteps;
            // Compute positions
            vec3 currentPoint(0.0, 0.0, 0.0);
            vec3 currentUTan(0.0, 0.0, 0.0);
            vec3 currentVTan(0.0, 0.0, 0.0);
            for (int i = 0; i <= 3; i++) {
                for (int j = 0; j <= 3; j++) {
                    // Getting Bernstein values.
                    float bu = bernstein( i, u );
                    float bv = bernstein( j, v );
                    float dbu = bernsteinDeriv( i, u );
                    float dbv = bernsteinDeriv( j, v );
                    float bu_bv = bu * bv;
                    float bu_dbv = bu * dbv;
                    float dbu_bv = dbu * bv;
                    // Add this control point's contribution onto the current point.
                    currentPoint[X] += patch[i][j][X] * bu_bv;
                    currentPoint[Y] += patch[i][j][Y] * bu_bv;
                    currentPoint[Z] += patch[i][j][Z] * bu_bv;
                    // Add this point's contribution to our u-tangent.
                    currentUTan[X] += patch[i][j][X] * dbu_bv;
                    currentUTan[Y] += patch[i][j][Y] * dbu_bv;
                    currentUTan[Z] += patch[i][j][Z] * dbu_bv;
                    // Add this point's contribution to our v-tangent.
                    currentVTan[X] += patch[i][j][X] * bu_dbv;
                    currentVTan[Y] += patch[i][j][Y] * bu_dbv;
                    currentVTan[Z] += patch[i][j][Z] * bu_dbv;
                }
            }
            // Saving point
            vertex[stepU][stepV] = currentPoint;
            // Calculate the vector normal given the tangent vectors
            // normal[stepU][stepV] = normalize( cross(currentUTan, currentVTan) );
            normal[stepU][stepV] = normalize( cross(currentVTan, currentUTan) );

            textureCoords[stepU][stepV] = vec2(u,v);
        }
    }

    // adding triangles to the mesh
    for(int m = 0; m < steps; m++) {
        for(int n = 0; n < steps; n++) {
            vec4 a = vec4(vertex[m  ][n  ],1.0);
            vec4 b = vec4(vertex[m+1][n  ],1.0);
            vec4 c = vec4(vertex[m+1][n+1],1.0);
            vec4 d = vec4(vertex[m  ][n+1],1.0);
            vec3 normalFace1 = normalize( cross(b - a, c - b) );
            vec3 normalFace2 = normalize( cross(a - d, c - a) );

            addVertex( a, normal[m  ][n  ], normalFace1, textureCoords[m  ][n  ] );
            addVertex( b, normal[m+1][n  ], normalFace1, textureCoords[m+1][n  ] );
            addVertex( c, normal[m+1][n+1], normalFace1, textureCoords[m+1][n+1] );

            addVertex( a, normal[m  ][n  ], normalFace2, textureCoords[m  ][n  ] );
            addVertex( d, normal[m  ][n+1], normalFace2, textureCoords[m  ][n+1] );
            addVertex( c, normal[m+1][n+1], normalFace2, textureCoords[m+1][n+1] );
        }
    }
    // adding the vector normal lines (for debug)
    for(int m = 0; m <= steps; m++) {
        for(int n = 0; n <= steps; n++) {
            normalArray.points.push_back(vec4(vertex[m][n],1.0));
            normalArray.points.push_back(vec4( vec3(vertex[m][n]) + normal[m][n], 1.0 ));
            normalArray.colors.push_back(vec3(1.0, 1.0, 1.0));
            normalArray.colors.push_back(vec3(1.0, 1.0, 1.0));
        }
    }

}


void generateMesh()
{
    mesh.points.clear();
    mesh.colors.clear();
    mesh.normals.clear();
    mesh.normalsFace.clear();
    mesh.textureCoords.clear();
    normalArray.points.clear();
    normalArray.colors.clear();
    normalArray.normals.clear();
    normalArray.normalsFace.clear();

    generateMeshFormula();

    std::cout << "Resolution: " << resolution << " Points: " << mesh.points.size() << std::endl;
    std::cout << "Normals: " << normalArray.points.size() << std::endl;
}





//--------------------------------------------------------------------------
bool loadControlPoints(std::string modelName)
{
    std::ifstream file;  
    float         x,y,z;
    int           index = 0;

    file.open(modelName.c_str());
    
    if(file.fail()) return false;
    
    std::string line;
    std::istringstream is;

    while(!file.eof()) {
        std::getline(file, line);
        if(line.empty()) continue;

        is.clear();
        is.str(line);
        is >> x;
        is >> y;
        is >> z;

        controlPoints[index] = vec4( x, y, z, 1.0 );
        colorCP[index] = vec3( 1.0, 1.0, 0.0 );
        index++;
    }

    return true;
}

void updateMarkedControlPoint()
{
    for ( int i = 0; i < 16; ++i ) {
        if ( i != markedControlPoint) colorCP[i] = vec3( 1.0, 1.0, 0.0 );
        else colorCP[i] = vec3( 1.0, 0.0, 0.0 );
    }

    glUseProgram( wireFrameShader );
    glBindBuffer( GL_ARRAY_BUFFER, controlPointsBuffer );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(controlPoints), sizeof(colorCP), colorCP );
}

//--------------------------------------------------------------------------


void updateShaderVariables(GLuint program)
{
    glUseProgram( program ); // set shader program

    glBindBuffer( GL_ARRAY_BUFFER, meshBuffer );
    glBufferData( GL_ARRAY_BUFFER, sizeofVec(mesh.points)+sizeofVec(mesh.colors)+
                                   sizeofVec(mesh.normals)+sizeofVec(mesh.normalsFace)+sizeofVec(mesh.textureCoords), 
                  NULL, GL_STATIC_DRAW );
    glBufferSubData( GL_ARRAY_BUFFER, 0, sizeofVec(mesh.points), &mesh.points[0] );
    glBufferSubData( GL_ARRAY_BUFFER, sizeofVec(mesh.points), sizeofVec(mesh.colors), &mesh.colors[0] );
    glBufferSubData( GL_ARRAY_BUFFER, sizeofVec(mesh.points)+sizeofVec(mesh.colors), sizeofVec(mesh.normals), &mesh.normals[0] );
    glBufferSubData( GL_ARRAY_BUFFER, sizeofVec(mesh.points)+sizeofVec(mesh.colors)+sizeofVec(mesh.normals), 
                     sizeofVec(mesh.normalsFace), &mesh.normalsFace[0] );
    glBufferSubData( GL_ARRAY_BUFFER, sizeofVec(mesh.points)+sizeofVec(mesh.colors)+sizeofVec(mesh.normals)+sizeofVec(mesh.normalsFace), 
                     sizeofVec(mesh.textureCoords), &mesh.textureCoords[0] );

    glBindBuffer( GL_ARRAY_BUFFER, controlPointsBuffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof(controlPoints) + sizeof(colorCP), NULL, GL_STATIC_DRAW );
    glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(controlPoints), controlPoints );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(controlPoints), sizeof(colorCP), colorCP );

    glBindBuffer( GL_ARRAY_BUFFER, axisBuffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof(axisPoints) + sizeof(axisColors), NULL, GL_STATIC_DRAW );
    glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(axisPoints), axisPoints );
    glBufferSubData( GL_ARRAY_BUFFER, sizeof(axisPoints), sizeof(axisColors), axisColors );

    glBindBuffer( GL_ARRAY_BUFFER, normalBuffer );
    glBufferData( GL_ARRAY_BUFFER, sizeofVec(normalArray.points)+sizeofVec(normalArray.colors), NULL, GL_STATIC_DRAW );
    glBufferSubData( GL_ARRAY_BUFFER, 0, sizeofVec(normalArray.points), &normalArray.points[0] );
    glBufferSubData( GL_ARRAY_BUFFER, sizeofVec(normalArray.points), sizeofVec(normalArray.colors), &normalArray.colors[0] );

    glUniform1i( glGetUniformLocation(program, "texture"), 0 );
    glUniform1i( glGetUniformLocation(program, "textureNorm"), 1 );
    glUniform1i( glGetUniformLocation(program, "textureDisplacement"), 2 );

    idvPosition = glGetAttribLocation( program, "vPosition" );
    idvColor = glGetAttribLocation( program, "vColor" );
    idvNormal = glGetAttribLocation( program, "vNormal" );
    idvNormFace = glGetAttribLocation( program, "vNormFace" );
    idvTextCoord = glGetAttribLocation( program, "vTextCoord" );

    idModelView  = glGetUniformLocation( program, "modelView" );
    idProjection = glGetUniformLocation( program, "projection" );
    idEyePos     = glGetUniformLocation( program, "eyePosition");

    idDisplacement = glGetUniformLocation( program, "maxDisplacement");
    idEnableDisp = glGetUniformLocation( program, "enableDisp");
    idEnableBump = glGetUniformLocation( program, "enableBump");
}

void initTexture()
{
    // Load image
    // imageFile = stbi_load("textures/wall.jpg", &textWidth, &textHeight, &textComp, 3);
    // imageNormFile = stbi_load("textures/wall_normal.jpg", &textWidth, &textHeight, &textComp, 3);
    // imageHeightFile = stbi_load("textures/wall_height.jpg", &textWidth, &textHeight, &textComp, 3);

    imageFile = stbi_load("textures/roof.bmp", &textWidth, &textHeight, &textComp, 3);
    imageNormFile = stbi_load("textures/roof_normal.bmp", &textWidth, &textHeight, &textComp, 3);
    imageHeightFile = stbi_load("textures/roof_height.bmp", &textWidth, &textHeight, &textComp, 3);

    // Initialize texture objects
    GLuint textures[3];
    glGenTextures( 3, textures );

    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_2D, textures[0] );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, textWidth, textHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, imageFile );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

    glActiveTexture( GL_TEXTURE1 );
    glBindTexture( GL_TEXTURE_2D, textures[1] );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, textWidth, textHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, imageNormFile );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );

    glActiveTexture( GL_TEXTURE2 );
    glBindTexture( GL_TEXTURE_2D, textures[2] );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB, textWidth, textHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, imageHeightFile );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
}

void init1()
{
    // Init texture
    initTexture();
    // Create a vertex array object
    GLuint vao[1];
    glGenVertexArrays( 1, vao );
    glBindVertexArray( vao[0] );

    // Create buffer objects
    glGenBuffers( 1, &meshBuffer );
    glGenBuffers( 1, &controlPointsBuffer );
    glGenBuffers( 1, &axisBuffer );
    glGenBuffers( 1, &normalBuffer );

    // Load shader programs
    wireFrameShader = InitShader( "shaders/bezierV.glsl", "shaders/bezierF.glsl" );
    smoothShadingShader = InitShader( "shaders/textureV.glsl", "shaders/textureF.glsl" );
    flatShadingShader = InitShader( "shaders/bezierV3.glsl", "shaders/bezierF.glsl" );

    loadControlPoints("patchPoints.txt");
    generateMesh();
    updateShaderVariables(smoothShadingShader);
    updateShaderVariables(flatShadingShader);
    updateShaderVariables(wireFrameShader);
    updateMarkedControlPoint();

    glEnable( GL_DEPTH_TEST );
    glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
    glPointSize( 8.0f );
    glClearColor( 0.3f, 0.3f, 0.32f, 1.0f );
}

//----------------------------------------------------------------------------
void display( void )
{
    // clear the window
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    vec3 centerModel(3.0, 3.0, 1.4);
    vec4  eye( (radius*sin(theta))+centerModel[0],
               (phi)+centerModel[1],
               (radius*cos(theta))+centerModel[2],
               1.0 );
    vec4  at( centerModel[0], centerModel[1], centerModel[2], 1.0 );
    vec4  up( 0.0, 1.0, 0.0, 0.0 );

    mat4 modelView = LookAt( eye, at, up );

    // Calculate projection transformation
    mat4 projection = Perspective( fovy, aspect, zNear, zFar );

    // update projection shader variables
    glUniformMatrix4fv( idModelView, 1, GL_TRUE, modelView );
    glUniformMatrix4fv( idProjection, 1, GL_TRUE, projection );
    


    // draw mesh ----------------------------------------------
    if (shading) {
        glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
        if (flatShading) updateShaderVariables( flatShadingShader );
        else updateShaderVariables( smoothShadingShader );
        glUniformMatrix4fv( idModelView, 1, GL_TRUE, modelView );
        glUniformMatrix4fv( idProjection, 1, GL_TRUE, projection );
        glUniform3fv( idEyePos, 1, eye );
    }
    glUniform1f( idDisplacement, displacement );
    glUniform1f( idEnableDisp, enableDisp );
    glUniform1f( idEnableBump, enableBump );

    glBindBuffer( GL_ARRAY_BUFFER, meshBuffer );
    glEnableVertexAttribArray( idvPosition );
    glVertexAttribPointer( idvPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
    glEnableVertexAttribArray( idvColor );
    glVertexAttribPointer( idvColor, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET( sizeofVec(mesh.points)) );
    glEnableVertexAttribArray( idvNormal );
    glVertexAttribPointer( idvNormal, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET( sizeofVec(mesh.points)+sizeofVec(mesh.colors)) );
    glEnableVertexAttribArray( idvNormFace );
    glVertexAttribPointer( idvNormFace, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET( sizeofVec(mesh.points)+sizeofVec(mesh.colors)+sizeofVec(mesh.normals)) );
    glEnableVertexAttribArray( idvTextCoord );
    glVertexAttribPointer( idvTextCoord, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET( sizeofVec(mesh.points)+sizeofVec(mesh.colors)+sizeofVec(mesh.normals)+sizeofVec(mesh.normalsFace)) );
    glDrawArrays( GL_TRIANGLES, 0, mesh.points.size() );

    if (shading) {
        glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
        updateShaderVariables( wireFrameShader );
    }

    // draw control points ----------------------------------------------
    // glBindBuffer( GL_ARRAY_BUFFER, controlPointsBuffer );
    // glEnableVertexAttribArray( idvPosition );
    // glVertexAttribPointer( idvPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
    // glEnableVertexAttribArray( idvColor );
    // glVertexAttribPointer( idvColor, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET( sizeof(controlPoints)) );
    // glDrawArrays( GL_POINTS, 0, 16 );

    // draw axis ----------------------------------------------
    // glBindBuffer( GL_ARRAY_BUFFER, axisBuffer );
    // glEnableVertexAttribArray( idvPosition );
    // glVertexAttribPointer( idvPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
    // glEnableVertexAttribArray( idvColor );
    // glVertexAttribPointer( idvColor, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET( sizeof(axisPoints)) );
    // glLineWidth( 4.0f );
    // glDrawArrays( GL_LINES, 0, 6 );
    // glLineWidth( 1.0f );

    // draw normals ----------------------------------------------
    if (showNormals) {
        glBindBuffer( GL_ARRAY_BUFFER, normalBuffer );
        glEnableVertexAttribArray( idvPosition );
        glVertexAttribPointer( idvPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
        glEnableVertexAttribArray( idvColor );
        glVertexAttribPointer( idvColor, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET( sizeofVec(normalArray.points)) );
        glDrawArrays( GL_LINES, 0, normalArray.points.size() );
    }

    glutSwapBuffers();
}

void updateMesh()
{
    generateMesh();
    updateShaderVariables(smoothShadingShader);
    updateShaderVariables(wireFrameShader);
}

void updateControlPoint(int axis, int direction) {
    controlPoints[markedControlPoint][axis] += direction;
    updateMesh();
}

//----------------------------------------------------------------------------
void keyboard( unsigned char key, int x, int y )
{
    switch ( key ) {
        case KEY_ESC:
            // free resources
            glDeleteProgram(wireFrameShader);
            glDeleteProgram(smoothShadingShader);
            glDeleteBuffers( 1, &meshBuffer );
            glDeleteBuffers( 1, &controlPointsBuffer );
            glDeleteBuffers( 1, &axisBuffer );
            exit( EXIT_SUCCESS );
            break;
        // case 'q':
        //     updateControlPoint(X_AXIS, -1);
        //     break;
        // case 'w':
        //     updateControlPoint(X_AXIS, 1);
        //     break;
        // case 'a':
        //     updateControlPoint(Y_AXIS, -1);
        //     break;
        // case 's':
        //     updateControlPoint(Y_AXIS, 1);
        //     break;
        // case 'z':
        //     updateControlPoint(Z_AXIS, -1);
        //     break;
        // case 'x':
        //     updateControlPoint(Z_AXIS, 1);
        //     break;
        case '1':
            if (enableBump == 1.0) enableBump = 0.0;
            else enableBump = 1.0;
            break;
        case '2':
            if (enableDisp == 1.0) enableDisp = 0.0;
            else enableDisp = 1.0;
            break;
        case '+':
            resolution++;
            if (resolution > MAX_RES ) resolution = MAX_RES;
            updateMesh();
            break;
        case '-':
            resolution--;
            if (resolution < 2 ) resolution = 2;
            updateMesh();
            break;
        case ',':
            displacement-=0.1;
            break;
        case '.':
            displacement+=0.1;
            break;
        // case ' ':
        //     markedControlPoint++;
        //     if ( markedControlPoint >= 16) markedControlPoint = 0;
        //     updateMarkedControlPoint();
        //     break;
        case 'm':
            shading = !shading;
            break;
        // case 'f':
        //     flatShading = !flatShading;
        //     break;
        // case 'n':
        //     showNormals = !showNormals;
        //     break;
    }
    // std::cout << key << ":" << (int)key << std::endl;
    glutPostRedisplay();
}

void onMouse( int button, int state, int x, int y ) 
{
    rotateCamera = false;
    zoomCamera = false;
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
        rotateCamera = true;
        lastMx = x;
        lastMy = y;
    }

    if ( button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
        zoomCamera = true;
        lastMx = x;
        lastMy = y;
    }
}
 
void onMotion( int x, int y )
{
    if (rotateCamera) {
        theta -= (x-lastMx)*0.01f;
        phi   += (y-lastMy)*0.1f;
    }

    if (zoomCamera) {
        radius += (y-lastMy)*0.01f;
        if ( radius < 1.0 ) radius = 1.0; // this should be the bounding box of the object
    }

    lastMx = x; 
    lastMy = y; 
    glutPostRedisplay(); 
}

//----------------------------------------------------------------------------
void reshape( int width, int height )
{
    glViewport( 0, 0, width, height );
    aspect = GLfloat(width)/height;
}

//----------------------------------------------------------------------------
int main( int argc, char **argv )
{
    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize( WINDOW_WIDTH, WINDOW_HEIGHT );

    // window 1
    glutCreateWindow( "Extra Project: Bump and Displacement Mapping" );
#ifndef __APPLE__  // Linux stuff
    glewExperimental = GL_TRUE; 
    glewInit(); 
#endif    
    init1();
    glutDisplayFunc( display );
    glutKeyboardFunc( keyboard );
    glutMouseFunc( onMouse );
    glutMotionFunc( onMotion );
    glutReshapeFunc( reshape );
    
    glutMainLoop();
    return 0;
}