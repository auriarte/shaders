// variables from buffer
attribute   vec4 vPosition;
attribute   vec3 vNormal;
attribute   vec3 vNormFace;

// variables from program
uniform     mat4 modelView;
uniform     mat4 projection;

// output values that will be interpreted per-fragment
varying  vec3 fNormal;
varying  vec3 fPosition;


void main()
{
    fNormal = vNormal;
    fPosition = vec3(vPosition);

	// Projection ---------------------------------------------
    gl_Position = projection * modelView * vPosition/vPosition.w;
}