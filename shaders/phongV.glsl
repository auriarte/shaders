// variables from buffer
attribute   vec4 vPosition;
attribute   vec3 vVertexNormal;
attribute   vec2 vTextCoord;

// variables from program
uniform     mat4 modelView;
uniform     mat4 projection;

// output values that will be interpreted per-fragment
varying  vec3 fNormal;
varying  vec3 fPosition;
varying  vec2 fTextCoord;

void main()
{
    fNormal = vVertexNormal;
    // fPosition = vec3(vPosition);
    fPosition = abs(vec3(vPosition));
    fTextCoord  = vTextCoord;

	// Projection ---------------------------------------------
    gl_Position = projection * modelView * vPosition/vPosition.w;
}