// variables from buffer
attribute   vec4 vPosition;
attribute   vec3 vNormal;
attribute   vec3 vColor;
attribute   vec2 vTextCoord;

// variables from program
uniform     mat4 modelView;
uniform     mat4 projection;
uniform sampler2D textureDisplacement;
uniform		float maxDisplacement;
uniform		float enableDisp;

// output values that will be interpreted per-fragment
varying  vec3 fColor;

void main()
{
    fColor = vColor;
    // gl_Position = projection * modelView * vPosition;

    // calculate vertex displacement
	float df = texture2D( textureDisplacement, vTextCoord.xy ).x;
	vec4 newVertexPos = vec4(vNormal * df * maxDisplacement, 0.0) + vPosition;

	if (enableDisp == 1.0) {
		gl_Position = projection * modelView * newVertexPos;
	} else {
		gl_Position = projection * modelView * vPosition;
	}
}