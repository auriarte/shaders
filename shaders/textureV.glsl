// variables from buffer
attribute   vec4 vPosition;
attribute   vec3 vNormal;
attribute   vec2 vTextCoord;

// variables from program
uniform     mat4 modelView;
uniform     mat4 projection;
uniform sampler2D textureDisplacement;
uniform		float maxDisplacement;
uniform		float enableDisp;

// output values that will be interpreted per-fragment
varying vec3 fPosition;
varying vec3 fNormal;
varying vec2 fTextCoord;

void main()
{
    fNormal = vNormal;
    fTextCoord  = vTextCoord;

    // calculate vertex displacement
	float df = texture2D( textureDisplacement, vTextCoord.xy ).x;
	vec4 newVertexPos = vec4(vNormal * df * maxDisplacement, 0.0) + vPosition/vPosition.w;

	if (enableDisp == 1.0) {
		fPosition = vec3(newVertexPos);
		gl_Position = projection * modelView * newVertexPos;
	} else {
		fPosition = vec3(vPosition);
		gl_Position = projection * modelView * vPosition/vPosition.w;
	}

    // fPosition = vec3(vPosition);
    // fPosition = vec3(newVertexPos);

	// Projection ---------------------------------------------
    // gl_Position = projection * modelView * vPosition/vPosition.w;
    // gl_Position = projection * modelView * newVertexPos;
}
