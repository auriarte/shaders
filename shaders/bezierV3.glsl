// variables from buffer
attribute   vec4 vPosition;
attribute   vec3 vNormal;
attribute   vec3 vNormFace;

// variables from program
uniform     mat4 modelView;
uniform     mat4 projection;
uniform 	vec3 eyePosition;

// internal variables
vec3 lightPosition = vec3(1.0, 1.0, -5.0);
vec4 lightDiffuse = vec4( 1.0, 1.0, 1.0, 1.0 );
vec4 materialDiffuse = vec4( 0.3, 0.3, 0.0, 1.0 );

// output values that will be interpreted per-fragment
varying  vec3 fColor;

void main()
{
	vec3 L = normalize( lightPosition );
    vec3 N = normalize( vNormFace );
    fColor =  vec3(materialDiffuse) * vec3(lightDiffuse) * max(dot(L, N), 0.0);

	// Projection ---------------------------------------------
    gl_Position = projection * modelView * vPosition/vPosition.w;
}