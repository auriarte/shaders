// variables from the vertex shader
varying vec3 fColor;

void main()
{
    gl_FragColor = vec4(fColor.xyz, 1.0);
}
