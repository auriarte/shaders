// variables from the vertex shader
varying  vec3 fPosition;
varying  vec3 fNormal;
varying  vec2 fTextCoord;

// variables from the program
uniform vec3 eyePosition;
uniform sampler3D texture;

// internal variables
// light
vec4 lightDifusse = vec4(1.0, 1.0, 1.0, 1.0);
vec4 lightSpecular = vec4(1.0, 1.0, 1.0, 1.0);
vec4 sceneAmbient = vec4(0.2, 0.2, 0.2, 1.0);
// material
vec4 matAmbinet = vec4( 1.0, 1.0, 1.0, 1.0 );
vec4 matDiffuse = vec4( 0.8, 0.8, 0.8, 1.0 );
vec4 matSpecular = vec4( 0.3, 0.3, 0.3, 1.0 );
float matShininess = 80.0;

// texture
vec3 lightWoodColor = vec3(0.6, 0.3, 0.1);
vec3 darkWoodColor = vec3(0.4, 0.2, 0.07);
vec3 noiseScale = vec3(0.0, 0.25, 1.0);
float uRingFreq = 6.0;
float noiseMag = 1.5;

vec3 Light()
{
    // Ambient light ---------------------------------------------
    // vec3 totalLighting = vec3(materialAmbient * sceneAmbient);
    vec3 totalLighting = vec3(matAmbinet * sceneAmbient);

    // Working with world coordinates
    // ----------------------------------------
    vec3 E = normalize( eyePosition - fPosition );
    vec3 L = normalize( eyePosition - fPosition );
    vec3 N = normalize( fNormal );
    vec3 H = normalize( L + E );
    
    // Diffuse light ---------------------------------------------
    // vec3 diffuse =  vec3(diffuseProduct1) * max(dot(L, N), 0.0);
    vec3 diffuse =  vec3(matDiffuse) * max(dot(L, N), 0.0);

    // Specular light ---------------------------------------------
    vec3 specular = vec3(0.0, 0.0, 0.0);
    if (dot(L, N) > 0.0) {  
        // specular = vec3(lightSpecular) * vec3(materialSpecular) * pow( max(dot(N, H), 0.0), shininess);
        specular = vec3(lightSpecular) * vec3(matSpecular) * pow( max(dot(N, H), 0.0), matShininess);
    }

    totalLighting += diffuse + specular;

    // gl_FragColor = vec4(totalLighting, 1.0);
    return totalLighting;
}

void main()
{
    vec4 noiseVec = noiseMag * texture3D(texture, noiseScale * fPosition);
    vec3 location = fPosition + noiseVec.rgb;

    //float dist = sqrt(location.x * location.x + location.z * location.z);
    float dist = length(location.xy); // optimized
    dist *= uRingFreq;

    float t = fract(dist + noiseVec.r + noiseVec.g + noiseVec.b) * 2.0;
    if (t > 1.0) {
        t = 2.0 - t;
    }

    vec4 color = vec4(mix(lightWoodColor, darkWoodColor, t), 1.0);

    // get light
    vec3 vLightIntensity = Light();

    color *= vec4(vLightIntensity, 1.0);
    gl_FragColor = color;
}
