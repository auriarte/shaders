// variables from the vertex shader
varying  vec3 fPosition;
varying  vec3 fNormal;

// variables from the program
uniform vec3 eyePosition;

// internal variables
vec3 lightPosition = vec3(3.0, 3.0, -5.0);

vec4 lightAmbient = vec4(0.2, 0.2, 0.2, 1.0);
vec4 lightDiffuse = vec4( 1.0, 1.0, 1.0, 1.0 );
vec4 lightSpecular = vec4(1.0, 1.0, 1.0, 1.0);

vec4 materialAmbient = vec4( 1.0, 1.0, 0.0, 1.0 );
vec4 materialDiffuse = vec4( 0.3, 0.3, 0.0, 1.0 );
vec4 materialSpecular = vec4( 1.0, 1.0, 0.0, 1.0 );
float shininess = 50.0;

void main()
{
    // Ambient light ---------------------------------------------
    vec3 ambient = vec3(materialAmbient * lightAmbient);

    // OPTION 2: Working with world coordinates
    // ----------------------------------------
    vec3 E = normalize( eyePosition - fPosition );
    vec3 L = normalize( lightPosition - fPosition );
    vec3 N = normalize( fNormal );
    vec3 H = normalize( L + E );
    
    // Diffuse light ---------------------------------------------
    vec3 diffuse =  vec3(materialDiffuse) * vec3(lightDiffuse) * max(dot(L, N), 0.0);

    // Specular light ---------------------------------------------
    vec3 specular = vec3(0.0, 0.0, 0.0);
    if (dot(L, N) > 0.0) {  
        specular = vec3(lightSpecular) * vec3(materialSpecular) * pow( max(dot(N, H), 0.0), shininess);
    }

    gl_FragColor = vec4((ambient + diffuse + specular), 1.0);
}
