// variables from the vertex shader
varying  vec3 fPosition;
varying  vec3 fNormal;
varying  vec2 fTextCoord;

// variables from the program
uniform vec3 eyePosition;
uniform sampler2D texture;
uniform sampler2D textureNorm;
uniform	float enableBump;

// internal variables
// light
vec4 lightDifusse = vec4(1.0, 1.0, 1.0, 1.0);
vec4 lightSpecular = vec4(1.0, 1.0, 1.0, 1.0);
vec4 sceneAmbient = vec4(0.2, 0.2, 0.2, 1.0);
// material
vec4 matAmbinet = vec4( 1.0, 1.0, 1.0, 1.0 );
vec4 matDiffuse = vec4( 0.8, 0.8, 0.8, 1.0 );
vec4 matSpecular = vec4( 0.3, 0.3, 0.3, 1.0 );
float matShininess = 80.0;

vec3 Light()
{
	//RGB of our normal map
	vec3 NormalMap = texture2D(textureNorm, fTextCoord).rgb;

    // Ambient light ---------------------------------------------
    // vec3 totalLighting = vec3(materialAmbient * sceneAmbient);
    vec3 totalLighting = vec3(matAmbinet * sceneAmbient);

    // Working with world coordinates
    // ----------------------------------------
    vec3 E = normalize( eyePosition - fPosition );
    vec3 L = normalize( eyePosition - fPosition );
    vec3 N;
    if (enableBump == 0.0) N = normalize( fNormal );
    else N = normalize( NormalMap * 2.0 - 1.0 );
    vec3 H = normalize( L + E );
    
    // Diffuse light ---------------------------------------------
    // vec3 diffuse =  vec3(diffuseProduct1) * max(dot(L, N), 0.0);
    vec3 diffuse =  vec3(matDiffuse) * max(dot(L, N), 0.0);

    // Specular light ---------------------------------------------
    vec3 specular = vec3(0.0, 0.0, 0.0);
    if (dot(L, N) > 0.0) {  
        // specular = vec3(lightSpecular) * vec3(materialSpecular) * pow( max(dot(N, H), 0.0), shininess);
        specular = vec3(lightSpecular) * vec3(matSpecular) * pow( max(dot(N, H), 0.0), matShininess);
    }

    totalLighting += diffuse + specular;

    // gl_FragColor = vec4(totalLighting, 1.0);
    return totalLighting;
}

void main() 
{ 
	vec4 diffuseColor = texture2D(texture, fTextCoord);

	// get light
    vec3 vLightIntensity = Light();

	gl_FragColor = diffuseColor * vec4(vLightIntensity, 1.0);
	// gl_FragColor =  vec4(vLightIntensity, 1.0);
	// gl_FragColor =  texture2D(textureDisplacement, fTextCoord);
	// gl_FragColor =  texture2D(texture, fTextCoord);
} 