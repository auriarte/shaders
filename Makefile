OS = $(shell uname)
ifeq ($(OS), Darwin)
	# MacOS options 
	GL_OPTIONS = -framework OpenGL -framework GLUT 
else
	# Linux options
	GL_OPTIONS = -lGLEW -lGL -lglut
endif

GCC_OPTIONS = -Wall -pedantic -I include
OPTIONS = $(GCC_OPTIONS) $(GL_OPTIONS)

CPP_FILES = $(wildcard *.cpp)
OUT_FILES = $(patsubst %.cpp, %, $(CPP_FILES))

.PHONY: clean initShader execute

.cpp:
	g++ $@.cpp Common/InitShader.o $(OPTIONS) -o $@

#Tell make to make one exe file for each .cpp file found in the current directory
all: clean initShader $(OUT_FILES) execute

initShader:
	g++ -c -I include Common/InitShader.cpp -o Common/InitShader.o

clean:
	rm -f $(OUT_FILES) Common/InitShader.o include/stb_image.o

execute:
	./main &
# ./mai2 &
