#include "Angel.h"
#include "pNoise.h"
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>

#define sizeofVec(stdVector) stdVector.size()*sizeof(stdVector[0])

//- CONSTANTS --------------------------------------------------------------
#define PI 3.141592654
#define TWOPI 6.283185308

#define WINDOW_WIDTH    500
#define WINDOW_HEIGHT   500

// ASCII KEYS CODE
#define KEY_ESC 033
#define KEY_SPC  32

enum { X = 0, Y = 1, Z = 2 };

struct vbo_t {
    std::vector<vec4> points;
    std::vector<vec3> faceNormals;
    std::vector<vec3> vertexNormals;
    std::vector<vec2> textureCoords;
};

struct material_t {
    vec4  ambient;
    vec4  diffuse;
    vec4  specular;
    float shininess;
};
// Material parameters
material_t materials[2];
// material_t material0 = {
//     vec4( 1.0, 1.0, 0.0, 1.0 ), // ambient
//     vec4( 0.3, 0.3, 0.0, 1.0 ), // diffuse
//     vec4( 1.0, 1.0, 0.0, 1.0 ), // specular
//     50.0f                       // shininess
// };
material_t material0 = {
    vec4( 1.0, 1.0, 1.0, 1.0 ), // ambient
    vec4( 0.7, 0.7, 0.7, 1.0 ), // diffuse
    vec4( 0.3, 0.3, 0.3, 1.0 ), // specular
    80.0f                       // shininess
};
material_t material1 = { // emerald
    vec4( 0.0215,  0.1745,   0.0215 ),  // ambient
    vec4( 0.07568, 0.61424,  0.07568 ), // diffuse
    vec4( 0.633,   0.727811, 0.633 ),   // specular
    76.8f                               // shininess
};
material_t material2 = {
    vec4( 0.2, 0.2, 0.2, 1.0 ), // ambient
    vec4( 1.0, 0.8, 0.8, 1.0 ), // diffuse
    vec4( 1.0, 1.0, 1.0, 1.0 ), // specular
    100.0f                      // shininess
};

// Lighting parameters
vec4 lightDiffuse1( 1.0, 1.0, 1.0, 1.0 );
vec4 lightDiffuse2( 1.0, 0.0, 0.0, 1.0 );

const int textureSize = 128;
GLuint textures;
GLubyte image[textureSize][textureSize][textureSize][3];

//- GLOBALS ----------------------------------------------------------------
vbo_t vbo[6];
GLuint buffer[6];
int bufferID;

GLuint shader[3];
int actualShader;

// Viewing transformation parameters
GLfloat radius = 1.0;
GLfloat theta  = 0.0;
GLfloat phi    = 0.0;

// Light transformation parameters
GLfloat lightTheta  = 0.0;
GLfloat lightPhi    = 0.0;

// Projection transformation parameters (Ortho)
GLfloat  left   = -1.0,  right = 1.0;
GLfloat  bottom = -1.0,  top   = 1.0;

// Projection transformation parameters (Perspective)
GLfloat  fovy = 45.0;  // Field-of-view in Y direction angle (in degrees)
GLfloat  aspect;       // Viewport aspect ratio

GLfloat  zNear  = 0.005, zFar  = 300.0;

// IDs of shader variable locations
GLuint idModelView, idProjection;
GLuint idDiffuse1, idDiffuse2, idAmbient, idSpecular, idShininess;
GLuint idLightPos, idEyePos;

// mouse camera control
int lastMx = 0, lastMy = 0;
bool rotateCamera = false;
bool zoomCamera = false;
bool rotateLight = false;

bool prespOrtho = false;
int materialID = 0;

//--------------------------------------------------------------------------
void sphereMap( vbo_t& buffer, vec4 vertex)
{
    float s,t;
    // radius = distance to origin (0,0,0)
    float radius = sqrt(vertex[X]*vertex[X]+vertex[Y]*vertex[Y]+vertex[Z]*vertex[Z]);
    t = acos(vertex[Z]/radius) / PI;
    // if (vertex[Y] >= 0)
        s = acos(vertex[X]/(radius * sin(PI*(t)))) / TWOPI;
    // else
        // s = (PI + acos(vertex[X]/(radius * sin(PI*(t))))) / TWOPI;

    buffer.textureCoords.push_back(vec2(s,t));
}

void addTriangle( vbo_t& buffer, const vec4& a, const vec4& b, const vec4& c, 
                  const vec3& aNormal, const vec3& bNormal, const vec3& cNormal, 
                  const vec3& normal )
{
    buffer.points.push_back(a); buffer.faceNormals.push_back(normal); buffer.vertexNormals.push_back(aNormal);
    buffer.points.push_back(b); buffer.faceNormals.push_back(normal); buffer.vertexNormals.push_back(bNormal);
    buffer.points.push_back(c); buffer.faceNormals.push_back(normal); buffer.vertexNormals.push_back(cNormal);
    sphereMap( buffer, a );
    sphereMap( buffer, b );
    sphereMap( buffer, c );
}

//--------------------------------------------------------------------------
bool loadModel(vbo_t& vbo, std::string modelName)
{
    std::ifstream file;  
    float           x,y,z;
    int             a,b,c;
    vec4            v1,v2,v3;
    std::string     str;
    std::vector<vec4> vertices;
    std::vector<vec3> faces;
    std::vector<vec3> faceNormals;
    std::vector<vec3> vertexNormals;
    std::vector<std::vector<int> > verticeFaces;
    int faceID = 0;

    file.open(modelName.c_str());
    
    if(file.fail()) return false;
    
    std::string line; 
    std::istringstream is;

    while(!file.eof()) {
        std::getline(file, line);
        if(line.empty()) continue;

        is.clear();
        is.str(line);       
        is >> str;      

        if (str.compare("v") == 0 ) { //vertex
            is >> x;  
            is >> y;
            is >> z;

            vertices.push_back(vec4(x,y,z,1.0));

        } else if (str.compare("f") == 0) { //face 
            if (verticeFaces.size() == 0) {
                verticeFaces.resize(vertices.size());
            }
            // parser vertex
            is >> a; v1 = vertices[a-1];
            is >> b; v2 = vertices[b-1];
            is >> c; v3 = vertices[c-1];

            // keep relation vertex - face
            verticeFaces.at(a-1).push_back(faceID);
            verticeFaces.at(b-1).push_back(faceID);
            verticeFaces.at(c-1).push_back(faceID);

            // add vertex to buffer
            faces.push_back(vec3(a,b,c));
            vec3  normal = normalize( cross(v2 - v1, v3 - v2) );
            faceNormals.push_back(normal);
            faceID++;
        }
    }

    file.close();

    // calculate vertex normal
    vec3 totalNormals;
    vec3 avgNormal;
    for (unsigned int i = 0; i < vertices.size(); i++) {
        totalNormals = vec3(0.0, 0.0, 0.0);
        for (unsigned int j = 0; j < verticeFaces[i].size(); j++) {
            vec3 faceNormal = faceNormals[verticeFaces[i][j]];
            totalNormals = totalNormals + faceNormal;
        }
        avgNormal = totalNormals / verticeFaces[i].size();
        vertexNormals.push_back(avgNormal);
    }

    // add faces to buffer
    for (unsigned int i = 0; i < faces.size(); i++) {
        addTriangle( vbo, vertices[faces[i].x-1], vertices[faces[i].y-1], vertices[faces[i].z-1], 
                     vertexNormals[faces[i].x-1], vertexNormals[faces[i].y-1], vertexNormals[faces[i].z-1],
                     faceNormals[i] );
    }

    return true;
}

//--------------------------------------------------------------------------
void createBufferModel( int bufferID, std::string modelName )
{
    loadModel(vbo[bufferID], modelName);
    
    // Initialize a buffer object
    glBindBuffer( GL_ARRAY_BUFFER, buffer[bufferID] );
    int pointsSize = sizeofVec(vbo[bufferID].points);
    int fNormalSize = sizeofVec(vbo[bufferID].faceNormals);
    int vNormalSize = sizeofVec(vbo[bufferID].vertexNormals);
    int vTextCoordSize = sizeofVec(vbo[bufferID].textureCoords);
    glBufferData( GL_ARRAY_BUFFER, pointsSize + fNormalSize + vNormalSize + vTextCoordSize, NULL, GL_STATIC_DRAW );
    glBufferSubData( GL_ARRAY_BUFFER, 0, pointsSize, &vbo[bufferID].points[0] );
    glBufferSubData( GL_ARRAY_BUFFER, pointsSize, fNormalSize, &vbo[bufferID].faceNormals[0] );
    glBufferSubData( GL_ARRAY_BUFFER, pointsSize+fNormalSize, vNormalSize, &vbo[bufferID].vertexNormals[0] );
    glBufferSubData( GL_ARRAY_BUFFER, pointsSize+fNormalSize+vNormalSize, vTextCoordSize, &vbo[bufferID].textureCoords[0] );
}

void setRender()
{
    GLuint program = shader[actualShader];
    glUseProgram( program ); // set shader program
    glBindBuffer( GL_ARRAY_BUFFER, buffer[bufferID] );  // set active buffer

    // Initialize the vertex position attribute from the vertex shader
    GLuint idPosition = glGetAttribLocation( program, "vPosition" );
    glEnableVertexAttribArray( idPosition );
    glVertexAttribPointer( idPosition, 4, GL_FLOAT, GL_FALSE, 0, 
                           BUFFER_OFFSET(0) );

    GLuint idFaceNormal = glGetAttribLocation( program, "vFaceNormal" );
    glEnableVertexAttribArray( idFaceNormal );
    glVertexAttribPointer( idFaceNormal, 3, GL_FLOAT, GL_FALSE, 0, 
                           BUFFER_OFFSET(sizeofVec(vbo[bufferID].points)) );

    GLuint idVertexNormal = glGetAttribLocation( program, "vVertexNormal" );
    glEnableVertexAttribArray( idVertexNormal );
    glVertexAttribPointer( idVertexNormal, 3, GL_FLOAT, GL_FALSE, 0, 
                           BUFFER_OFFSET(sizeofVec(vbo[bufferID].points) + sizeofVec(vbo[bufferID].faceNormals)) );

    GLuint idvTextCoord = glGetAttribLocation( program, "vTextCoord" );
    glEnableVertexAttribArray( idvTextCoord );
    glVertexAttribPointer( idvTextCoord, 2, GL_FLOAT, GL_FALSE, 0, 
                           BUFFER_OFFSET(sizeofVec(vbo[bufferID].points)+sizeofVec(vbo[bufferID].faceNormals)+sizeofVec(vbo[bufferID].vertexNormals)) );
    
    glUniform1i( glGetUniformLocation(program, "texture"), 0 );

    // get shader variables locations (-1 means that the variable doesn't exist)
    idModelView  = glGetUniformLocation( program, "modelView" );
    idProjection = glGetUniformLocation( program, "projection" );

    idDiffuse1   = glGetUniformLocation( program, "diffuseProduct1");
    idDiffuse2   = glGetUniformLocation( program, "diffuseProduct2");
    idAmbient    = glGetUniformLocation( program, "materialAmbient");
    idSpecular   = glGetUniformLocation( program, "materialSpecular");
    idShininess  = glGetUniformLocation( program, "shininess");
    idLightPos   = glGetUniformLocation( program, "lightPosition");
    idEyePos     = glGetUniformLocation( program, "eyePosition");
}

void initTexture()
{
    perlinInit();
    float x,y,z;
    for ( int i = 0; i < textureSize; i++ ) {
        x = (float)i/textureSize;
        for ( int j = 0; j < textureSize; j++ ) {
            y = (float)j/textureSize;
            for ( int k = 0; k < textureSize; k++ ) {
                z = (float)k/textureSize;
                // float g = perlinNoise(x,y,z); // the range of perlinNoise is [-1,1]
                float g = turb(x, y, z, 5, 4, 4);
                g = (g+1)/2; // now the range is [0,1]
                // std::cout << "g = " << std::fixed << std::setprecision(2) << g << std::endl;
                image[i][j][k][0] = (GLubyte)(g*255);
                image[i][j][k][1] = (GLubyte)(g*255);
                image[i][j][k][2] = (GLubyte)(g*255);
            }
        }
    }

    // Initialize texture objects
    glGenTextures( 1, &textures );

    glActiveTexture( GL_TEXTURE0 );
    glBindTexture( GL_TEXTURE_3D, textures );
    glTexImage3D( GL_TEXTURE_3D, 0, GL_RGB, textureSize, textureSize, textureSize, 0,
                  GL_RGB, GL_UNSIGNED_BYTE, image );
    glTexParameterf( GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameterf( GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );

    // glEnable(GL_TEXTURE_3D);
}

void init1()
{
    initTexture();
    // Define materials array
    materials[0] = material0;
    materials[1] = material1;
    materials[2] = material2;

    // Create a vertex array object
    GLuint vao[1];
    glGenVertexArrays( 1, vao );
    glBindVertexArray( vao[0] );

    // Create buffer object for each model
    glGenBuffers( 6, buffer );
    createBufferModel(0, "models/bound-lo-sphere.smf");
    createBufferModel(1, "models/bound-bunny_5k.smf");
    createBufferModel(2, "models/bound-cow.smf");
    createBufferModel(3, "models/teapot.smf");
    createBufferModel(4, "models/frog.smf");
    createBufferModel(5, "models/bunny_69k.smf");

    // Load shader programs
    // shader[0] = InitShader( "shaders/flatV.glsl", "shaders/flatF.glsl" );
    // shader[1] = InitShader( "shaders/gouradV.glsl", "shaders/gouradF.glsl" );
    shader[2] = InitShader( "shaders/phongV.glsl", "shaders/phongF.glsl" );

    bufferID = 1;
    actualShader = 2;
    setRender();
  
    glEnable( GL_DEPTH_TEST );
    glClearColor( 0.3f, 0.3f, 0.32f, 1.0f );
}

//----------------------------------------------------------------------------
void display( void )
{
    // clear the window
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

    vec4  eye( radius*sin(theta),
               phi,
               radius*cos(theta),
               1.0 );
    vec4  at( 0.0, 0.0, 0.0, 1.0 ); // this should be the center of bounding box of the model
    vec4  up( 0.0, 1.0, 0.0, 0.0 );

    mat4  modelView = LookAt( eye, at, up );

    // Calculate projection transformation
    mat4  projection;
    if (prespOrtho) {
        projection = Ortho( left, right, bottom, top, zNear, zFar );
    } else {
        projection = Perspective( fovy, aspect, zNear, zFar );
    }

    // update projection shader variables
    glUniformMatrix4fv( idModelView, 1, GL_TRUE, modelView );
    glUniformMatrix4fv( idProjection, 1, GL_TRUE, projection );

    // update material and light shader variables
    vec4 diffuseProduct1 = lightDiffuse1 * materials[materialID].diffuse;
    vec4 diffuseProduct2 = lightDiffuse2 * materials[materialID].diffuse;
    glUniform4fv( idDiffuse1, 1, diffuseProduct1 );
    glUniform4fv( idDiffuse2, 1, diffuseProduct2 );

    glUniform4fv( idAmbient, 1, materials[materialID].ambient );
    glUniform4fv( idSpecular, 1, materials[materialID].specular );
    glUniform1f(  idShininess, materials[materialID].shininess );

    vec3 lightPosition( 5*sin(lightTheta), lightPhi, 5*cos(lightTheta));
    glUniform3fv( idLightPos, 1, lightPosition );
    glUniform3fv( idEyePos, 1, eye );

    // draw the points in buffer ----------------------------------------------
    glDrawArrays( GL_TRIANGLES, 0, vbo[bufferID].points.size() );
    glutSwapBuffers();
}

//----------------------------------------------------------------------------
void keyboard( unsigned char key, int x, int y )
{
    switch ( key ) {
        case KEY_ESC:
            // free resources
            glDeleteProgram(shader[0]);
            glDeleteProgram(shader[1]);
            glDeleteProgram(shader[2]);
            glDeleteBuffers( 6, buffer );
            exit( EXIT_SUCCESS );
            break;
        case '1': bufferID = 0; setRender(); break;
        case '2': bufferID = 1; setRender(); break;
        case '3': bufferID = 2; setRender(); break;
        case '4': bufferID = 3; setRender(); break;
        case '5': bufferID = 4; setRender(); break;
        case '6': bufferID = 5; setRender(); break;
        case 'p': prespOrtho = !prespOrtho; break;
        case 's': 
            actualShader++;
            if ( actualShader > 2 ) actualShader = 1;
            setRender();
            break;
        case 'm': 
            materialID++;
            if ( materialID > 2 ) materialID = 0;
            break;
    }
    // std::cout << key << ":" << (int)key << std::endl;
    glutPostRedisplay();
}

void onMouse( int button, int state, int x, int y ) 
{
    rotateCamera = false;
    zoomCamera = false;
    rotateLight = false;
    if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
        rotateCamera = true;
        lastMx = x;
        lastMy = y;
    }

    if ( button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN) {
        zoomCamera = true;
        lastMx = x;
        lastMy = y;
    }

    if ( button == 1 && state == GLUT_DOWN) {
        rotateLight = true;
        lastMx = x;
        lastMy = y;
    }
}
 
void onMotion( int x, int y )
{
    if (rotateCamera) {
        theta -= (x-lastMx)*0.01f;
        phi   += (y-lastMy)*0.01f;
    }

    if (zoomCamera) {
        radius += (y-lastMy)*0.01f;
        if ( radius < 1.0 ) radius = 1.0; // this should be the bounding box of the object
    }

    if (rotateLight) {
        lightTheta += (x-lastMx)*0.01f;
        lightPhi   -= (y-lastMy)*0.01f;
    }
    lastMx = x; 
    lastMy = y; 
    glutPostRedisplay(); 
}

//----------------------------------------------------------------------------
void reshape( int width, int height )
{
    glViewport( 0, 0, width, height );
    aspect = GLfloat(width)/height;
}

//----------------------------------------------------------------------------
int main( int argc, char **argv )
{
    glutInit( &argc, argv );
    glutInitDisplayMode( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH );
    glutInitWindowSize( WINDOW_WIDTH, WINDOW_HEIGHT );

    // window 1
    glutCreateWindow( "Assignment 6 - Main Window" );
#ifndef __APPLE__  // Linux stuff
    glewExperimental = GL_TRUE; 
    glewInit(); 
#endif    
    init1();
    glutDisplayFunc( display );
    glutKeyboardFunc( keyboard );
    glutMouseFunc( onMouse );
    glutMotionFunc( onMotion );
    glutReshapeFunc( reshape );
    
    glutMainLoop();
    return 0;
}
