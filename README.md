# DESCRIPTION

Shows a Bezier patch with a bitmap texture and/or Normal/Displacement Mapping

Mouse:

* Left mouse clicked and move to change the position of the camera.
* Right mouse clicked and move to change the zoom.

Keyboards:

* Esc: close the window.
* 1: ON/OFF Normal (Bump) Mapping
* 2: ON/OFF Displacement Mapping
* -: Decrement number of vertexes
* +: Increment number of vertexes
* ,: Decrement displacement scale
* .: Increment displacement scale
* m: ON/OFF wireframe render

# ENVIRONMENT

Developed under macOS, tested in Ubuntu.

# EXECUTION

Just execute "make" and the Makefile will compile the files and execute the window applications.

# SCREENSHOTS

No shaders
![normal.png](https://bitbucket.org/repo/Az6ayq/images/1749888091-normal.png)

Bump mapping
![bump.png](https://bitbucket.org/repo/Az6ayq/images/1433249546-bump.png)

Displacement mapping 
![displacement.png](https://bitbucket.org/repo/Az6ayq/images/1658972583-displacement.png)